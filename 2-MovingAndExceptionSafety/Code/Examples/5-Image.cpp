#include <vector>
#include <string>
#include <fstream>

class Image
{
public:
    Image(const std::string& filename) :
        filename_(filename)
    {
        std::ifstream reader(filename);
        if (!reader.is_open())
            throw std::invalid_argument("Cannot read image: file " + filename + " does not exist");
        
        auto filesize = reader.tellg();    // go to end of file
        data_.resize(filesize);          // this can throw, too!

        reader.seekg(0, std::ios::beg);         // set cursor back to the beginning
        reader.read(&data_[0], filesize);     
    }

    // Remember, this creates
    // by default    
    Image(const Image& other) :
        filename_(other.filename),
        data_(other.data_)
    {
    }

    // and:
    Image& operator=(const Image& other)
    {
        filename_ = other.filename_;
        data_     = other.data_;
        return *this;
    }
    // why is this bad?



private:
    std::string filename_;  
    std::vector<char> data_;  // can be a lot of data (big image!)
};

int main()
{
    Image a("SomeImage.png");
    Image b("SomeOtherImage.png");

    try {
        b = a;
    } catch (std::exception& e)
    {
        std::cout << "Oh noes " << e.what() << std:endl;
        // What is the state of b now?
    }

}



