#include <vector>
#include <list>
#include <string>
#include <fstream>
#include <iostream>


std::vector<char> readDataFromFile(const std::string& filename)
{
    std::ifstream reader(filename);
    if (!reader.is_open())
        throw std::invalid_argument("Cannot read image: file " + filename + " does not exist");
    
    auto filesize = reader.tellg();    // go to end of file
    std::vector<char> data(filesize);

    reader.seekg(0, std::ios::beg);         // set cursor back to the beginning
    reader.read(&data_[0], filesize); 

    return data;
}


class Image
{
public:
    Image(const std::string& filename) :
        filename_(filename),
        data_( readDataFromFile(filename) )
    {
        std::cout << "Constructor" << std::endl;
    }

    // Remember, this creates
    // by default    
    Image(const Image& other) :
        filename_(other.filename_),
        data_(other.data_)
    {
        std::cout << "Copy-constructor" << std::endl;
    }

    Image(Image&& other) :
        Image()
    {
        std::cout << "Move constructor" << std::endl;
        swap(*this, other);
    }

    void replaceData(const std::string& newfilename)
    {
        data_ = readDataFromFile(newfilename);
    }

    // and:
    Image& operator=(Image other) // <-- Note the copy in the function
    {
        std::cout << "Copy-assign" << std::endl;
        swap(*this, other);
        return *this;
    }

    friend void swap(Image& first, Image& other)
    {
        using std::swap;
        swap(first.filename_, other.filename_);
        swap(first.data_, other.data_);
    }
    
    std::string getFilename() const 
    { 
        cache_ = "SomeData";
        return filename_; 
    }

    

    void setFilename(const std::string& filename)
    {
        filename_ = filename;
    }

private:
    Image() : filename_(), data_() {}
    mutable std::string cache_;
    std::string filename_;  
    std::vector<char> data_;  // can be a lot of data (big image!)
};


Image loadImage(const std::string& filename)
{
    Image img(filename);
    return img; //Return-value optimization omits copy-constructor, even though it "legally" should use it
}


int main()
{
    std::cout << "--> Load image and store in local variable" << std::endl;
    Image img = loadImage("10-Image.cpp");
    std::cout << "--> Successfully loaded image" << std::endl;

    std::list<Image> images;

    std::cout << "--> Create image into vector" << std::endl;
    images.push_back( loadImage("7-Image.cpp") );

    std::cout << "--> Put image into vector" << std::endl;
    images.push_back( std::move(img) );

    const Image a  = loadImage("10-Image.cpp");
    std::cout << "Filename: " << a.getFilename() << std::endl;


    

    return 0;
}



