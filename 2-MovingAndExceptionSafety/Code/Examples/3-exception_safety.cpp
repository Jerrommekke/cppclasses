class Image
{
public:
    Image(const std::string& filename) :
        filename_(filename)
    {
        std::ifstream reader(filename);
        if (!reader.is_open())
            throw std::invalid_argument("Cannot read image: file " + filename + " does not exist");
        
        auto filesize = reader.tellg();    // go to end of file
        data_.resize(filesize);          // this can throw, too!

        reader.seekg(0, std::ios::beg);         // set cursor back to the beginning
        reader.read(&data_[0], filesize);     
    }

private:
    std::string filename_;  
    std::vector<char> data_;  // can be a lot of data (big image!)
};

class Gallery
{
public:
    ... 
    // Gallery always shows N images, and buffers the current image so that it's available in higher detail
    // FIFO 
    const Image& currentImage()
    {
        return images_[currentImage_];
    }
    const Image& showImage(int i)
    {
        currentImage_ = i;
        return currentImage();
    }
    // Now has a strong exception guarantee
    void addImage(const std::string& filename)
    {
        // FIFO  
        // We no longer need to explicity call delete!
        images_.push_back( Image(filename));
        images_.pop_front();
        currentImage = images_.size() - 1;
    }


private:
    std::list< Image > images_;
    int currentImage_; 

};




