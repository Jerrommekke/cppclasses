#include <vector>
#include <string>
#include <fstream>

class Image
{
public:
    Image(const std::string& filename) :
        filename_(filename)
    {
        std::ifstream reader(filename);
        if (!reader.is_open())
            throw std::invalid_argument("Cannot read image: file " + filename + " does not exist");
        
        auto filesize = reader.tellg();    // go to end of file
        data_.resize(filesize);          // this can throw, too!

        reader.seekg(0, std::ios::beg);         // set cursor back to the beginning
        reader.read(&data_[0], filesize);     
    }

    // Remember, this creates
    // by default    
    Image(const Image& other) :
        filename_(other.filename),
        data_(other.data_)
    {
    }

    // and:
    Image& operator=(Image other) // <-- Note the copy in the function
    {
        swap(this, other);
        return *this;
    }

    
    friend void swap(Image& first, Image& other)
    {
        using std::swap;
        swap(first.filename_, other.filename_);
        swap(first.data_, other.data_);
    }
    
private:
    std::string filename_;  
    std::vector<char> data_;  // can be a lot of data (big image!)
};

int main()
{
    Image a("a");
    Image b("b");
    swap(a, b);
    
}



