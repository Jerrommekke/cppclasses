#include <vector>
#include <string>
#include <fstream>

class Image
{
public:
    Image(const std::string& filename) :
        filename_(filename)
    {
        std::ifstream reader(filename);
        if (!reader.is_open())
            throw std::invalid_argument("Cannot read image: file " + filename + " does not exist");
        
        auto filesize = reader.tellg();    // go to end of file
        data_.resize(filesize);          // this can throw, too!

        reader.seekg(0, std::ios::beg);         // set cursor back to the beginning
        reader.read(&data_[0], filesize);     
    }

    // Remember, this creates
    // by default    
    Image(const Image& other) :
        filename_(other.filename),
        data_(other.data_)
    {
    }

    // and:
    Image& operator=(const Image& other)
    {
        // What this does, internally is:
        filename_ = other.filename_;
        data_.resize(other.data_.size());  // <--- may throw!
        ::memcpy( data_.data(), other.data(), sizeof(char)*data_.size() );

        return *this;
        // What is the exception guarantee of this function?
        // how do we fix this?
    }


private:
    std::string filename_;  
    std::vector<char> data_;  // can be a lot of data (big image!)
};

int main()
{
    Image a("Hello.png");
    
    std::list<Image> images;
    images.push_back(a);
    images.push_back( Image("a.png"));
    images.push_back( Image("b.png"));

    for (Image& im : images)
    {
        im = images.front();
    }

    

}



