#include <vector>
#include <string>
#include <fstream>
#include "sqlite3.h"

class Database
{ // C++ wrapper around sqlite API
public:
    Database(const std::string& filename) :
        filename_(filename)
    {
        auto responseCode = sqlite3_open(filename.c_str(), &database_);
        if(responseCode != SQLITE_OK) {
            sqlite3_close(database_);
            throw std::invalid_argument("Could not open database " + filename); 
        }   
    }

    // Delete copy constructor and copy-assignment, since there's no logical copy of a 
    // an object that represents a view on a file in a filesystem
    Database(const Database& other) = delete;
    Database& operator=(const Database& other) = delete; 

    // Move constructor!
    Database(Database&& other) :
        Database()
    {
        swap(*this, other);
    }


    ~Database()
    {
        if (database_) sqlite3_close(database_);
    }

    friend void swap(Database& first, Database& other)
    {
        using std::swap;
        swap(first.filename_, other.filename_);
        swap(first.database_, other.database_);
    }
    
private:
    Database() :
        filename_(), database_(nullptr)
    {
    }

    std::string filename_;  
    sqlite3* database_; // We need to cope with the API available!
};

Database createDatabase(const std::string& filename)
{  
    Database db(filename);
    // Do some configuration
    // db.configure(...)
    // ... 
    return db;
}

int main()
{

    Database db = createDatabase("database.sqlite");

    std::vector<Database> databases;
    databases.push_back( std::move(db) );

    // i still have a reference db
    // !! db is now invalid
    // i also have a reference to databases.front()
}



