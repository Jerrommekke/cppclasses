#include <iostream>
#include <fstream>

#include "Card_JW.h"
#include "Hand_JW.h"


using namespace std;
using namespace Euler54;

int jamahl()
{
    ifstream file("poker.txt");
    int player1Wins = 0;
    for (string line; getline(file, line);) {
        Hand_JW one(line.substr(0, 14));
        Hand_JW two(line.substr(15, 14));
        if (one > two) {
            player1Wins++;
        }
    }
    return player1Wins;
}


