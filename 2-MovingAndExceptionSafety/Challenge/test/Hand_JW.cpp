//
// Created by Jamahl Watson on 15/12/2017.
//

#include <sstream>
#include <map>
#include "Hand_JW.h"

namespace Euler54
{

//    High Card_JW: Highest value card.                                 2-14
//    One Pair: Two cards of the same value.                            15-29
//    Two Pairs: Two different pairs.                                   30
//    Three of a Kind: Three cards of the same value.                   45
//    Straight: All cards are consecutive values.                       60
//    Flush: All cards of the same suit.                                75
//    Full House: Three of a kind and a pair.                           90
//    Four of a Kind: Four cards of the same value.                     105
//    Straight Flush: All cards are consecutive values of same suit.    120
//    Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.           135

    struct BidirectionalMap
    {
        std::map<int, int> cardValuesToFrequency;
        std::map<int, std::vector<int>> frequencyToCardValues;
    };

    inline BidirectionalMap getValueFrequency(const std::vector<Card_JW> &cards)
    {
        BidirectionalMap result = { std::map<int, int>(), std::map<int, std::vector<int>>() };

        for ( auto card : cards ) {
            int val = card.getValue();

            auto lowerBound = result.cardValuesToFrequency.lower_bound(val);
            if ( lowerBound != result.cardValuesToFrequency.end() && !(result.cardValuesToFrequency.key_comp()(val, lowerBound->first)) )
            {
                lowerBound->second++;
            }
            else
            {
                result.cardValuesToFrequency.insert(lowerBound, std::map<int, int>::value_type(val, 1));
            }
        }

        for (const auto &value : result.cardValuesToFrequency) {
            result.frequencyToCardValues[value.second].push_back(value.first);
        }

        return std::move(result);
    }

    struct RoyalFlushFlushOrStraight
    {
        bool isRoyalFlush;
        bool isFlush;
        bool isStraight;
    };

    inline bool allInSequence(const std::vector<Card_JW>& cards)
    {
        for ( int i = 0; i < 4; ++i )
            if ( !cards[i].isAdjacent(cards[i+1]) )
                return false;
        return true;

    }

    inline bool allSameSuit(const std::vector<Card_JW>& cards)
    {
        char suit(cards[0].getSuit());
        for ( int i = 1; i < 5; ++i )
            if ( suit != cards[i].getSuit() )
                return false;
        return true;
    }

    inline RoyalFlushFlushOrStraight checkRoyalFlushFlushOrStraight(const std::vector<Card_JW>& cards)
    {
        RoyalFlushFlushOrStraight ans = {false, allSameSuit(cards), allInSequence(cards)};
        ans.isRoyalFlush = ( cards[0].getValue() == 14 && ans.isFlush && ans.isStraight );
        return ans;
    }

    inline std::vector<Card_JW> stringToHand(const std::string &in)
    {
        std::stringstream stream(in);
        std::vector<Card_JW> cards;
        for ( int i = 0; i < 5; ++i ) {
            Card_JW tmp;
            stream >> tmp;
            cards.push_back(tmp);
        }
        return cards;
    }

    std::ostream& operator<<(std::ostream& out, const Hand_JW& hand)
    {
        for ( auto card : hand.cards_) {
            out << card;
        }
        return out;
    }

    inline int Hand_JW::parseHandFromValue(std::vector<Card_JW> &cards)
    {
        std::sort(cards.begin(), cards.end(), std::greater<>());

        int highCard = cards[0].getValue();

        RoyalFlushFlushOrStraight royalFlushFlushOrStraight = checkRoyalFlushFlushOrStraight(cards);

        if ( royalFlushFlushOrStraight.isRoyalFlush ) {
            return 135;
        }
        else if ( royalFlushFlushOrStraight.isStraight && royalFlushFlushOrStraight.isFlush ) {
            return 120 + highCard;
        }
        BidirectionalMap values = getValueFrequency(cards);
        auto fourOfAKind = values.frequencyToCardValues.find(4);
        if ( fourOfAKind != values.frequencyToCardValues.end() ) {
            return 105 + fourOfAKind->second[0];
        }

        auto threeOfAKind = values.frequencyToCardValues.find(3);
        auto pairs = values.frequencyToCardValues.find(2);

        bool hasPairs = pairs != values.frequencyToCardValues.end();
        bool isThreeOfAKind = threeOfAKind != values.frequencyToCardValues.end();

        if ( isThreeOfAKind && hasPairs ) {
            return 90 + ((threeOfAKind->second[0] > pairs->second[0]) ? threeOfAKind->second[0] : pairs->second[0]);
        } else if ( royalFlushFlushOrStraight.isFlush ) {
            return 75 + highCard;
        } else if ( royalFlushFlushOrStraight.isStraight ) {
            return 60 + highCard;
        } else if ( isThreeOfAKind ) {
            return 45 + threeOfAKind->second[0];
        } else if ( hasPairs )
            if ( pairs->second.size() == 2 )
                return 30 + (pairs->second[0] > pairs->second[1] ? pairs->second[0] : pairs->second[1]);
            else
                return 15 + pairs->second[0];
        else {
            return highCard;
        }
    }

    Hand_JW::Hand_JW(const std::string &cards) :
        cards_(stringToHand(cards)),
        value_(parseHandFromValue(cards_))
    {}

    Hand_JW::Hand_JW(std::vector<Card_JW> cards) :
        cards_(std::move(cards)),
        value_(parseHandFromValue(cards_))
    {}

    Hand_JW::Hand_JW(Card_JW first, Card_JW second, Card_JW third, Card_JW fourth, Card_JW fifth) :
        cards_({ first, second, third, fourth, fifth}),
        value_(parseHandFromValue(cards_))
    {}

    bool Hand_JW::operator==(const Hand_JW &other) const {
        return this->value_ == other.value_;
    }

    bool Hand_JW::operator!=(const Hand_JW &other) const {
        return this->value_ != other.value_;
    }

    bool Hand_JW::operator<(const Hand_JW &other) const
    {
        return this->value_ < other.value_;
    }

    bool Hand_JW::operator>(const Hand_JW &other) const
    {
        return this->value_ > other.value_;
    }

    bool Hand_JW::operator<=(const Hand_JW &other) const
    {
        return this->value_ <= other.value_;
    }

    bool Hand_JW::operator>=(const Hand_JW &other) const
    {
        return this->value_ >= other.value_;
    }
}