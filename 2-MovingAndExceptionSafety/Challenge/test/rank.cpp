#include "nick.h"
#include "nick_2.h"
#include "salvatore.h"
#include "s2.h"
#include "jamahl.h"

#include "gtest/gtest.h"

TEST(Euler, Nick)
{
    EXPECT_EQ(nick(), 37976 );
}

TEST(Euler, Nick_2)
{
    EXPECT_EQ(nick_2(), 37976 );
}

TEST(Euler, Salvatore)
{
    EXPECT_EQ(salvatore(), 37976 );
}

TEST(Euler, Salvatore_2)
{
    EXPECT_EQ(s2::salvatore_2(), 37976 );
}

TEST(Euler, Jamahl)
{
    EXPECT_EQ(jamahl(), 37976 );

}
