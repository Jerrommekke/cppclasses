//
// Created by Jamahl Watson on 15/12/2017.
//

#ifndef EULER54_HAND_H
#define EULER54_HAND_H

#include <vector>

#include "Card_JW.h"

namespace Euler54 {
    class Hand_JW {
    public:
        explicit Hand_JW(const std::string &cards);
        explicit Hand_JW(std::vector<Card_JW> cards);
        Hand_JW(Card_JW first, Card_JW second, Card_JW third, Card_JW forth, Card_JW fifth);
        friend std::ostream& operator<<(std::ostream& out, const Hand_JW& hand);

        bool operator==(const Hand_JW &other) const;
        bool operator!=(const Hand_JW &other) const;
        bool operator<(const Hand_JW &other) const;
        bool operator>(const Hand_JW &other) const;
        bool operator<=(const Hand_JW &other) const;
        bool operator>=(const Hand_JW &other) const;
    private:
        int parseHandFromValue(std::vector<Card_JW> &cards);

        std::vector<Card_JW> cards_;
        int value_;
    };
}

#endif //EULER54_HAND_H
