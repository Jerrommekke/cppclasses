//
//

#ifndef POKER_GAME_CARD_MATCHER_H_H
#define POKER_GAME_CARD_MATCHER_H_H

#include <vector>
#include "Card.h"

enum HandScore {
    none,
    onePair = 2,
    twoPairs,
    threeOfAKind,
    straight,
    flush,
    fullHouse,
    fourOfAKind,
    straightFlush,
    royalFlush
};

const static int PLAYER_ONE_WIN = 1;
const static int PLAYER_TWO_WIN = -1;

Card::HandsCollection readRawHandsCollection(const Card::HandsRawCollection& rawCollection);

long long countWins(const Card::HandsCollection& playerOneHands,
                    const Card::HandsCollection& playerTwoHands);

#endif //POKER_GAME_CARD_MATCHER_H_H
