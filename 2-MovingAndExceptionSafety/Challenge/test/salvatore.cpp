#include <iostream>
#include <fstream>
#include <sstream>
#include "salvatore_matcher.h"

inline std::pair<Card::HandsRawCollection, Card::HandsRawCollection> readStringsFromFile(const std::string& filePath) {

    Card::HandsRawCollection playerOneHands;
    Card::HandsRawCollection playerTwoHands;

    std::ifstream inStream(filePath);
    std::string line;

    while(std::getline(inStream, line)){
        std::istringstream buf(line);
        std::istream_iterator<std::string> beg(buf), end;
        std::vector<std::string> tokens(beg, end);
        auto handSize = (int) (tokens.size() / 2);
        std::vector<std::string> playerOne(tokens.begin(), tokens.begin() + handSize);
        std::vector<std::string> playerTwo(tokens.begin() + handSize, tokens.end());

        playerOneHands.push_back(playerOne);
        playerTwoHands.push_back(playerTwo);
    }

    return std::pair<Card::HandsRawCollection,Card::HandsRawCollection>(playerOneHands, playerTwoHands);
}

int salvatore()
{
    std::pair<Card::HandsRawCollection, Card::HandsRawCollection> rawHands = readStringsFromFile("poker.txt");

    Card::HandsCollection playerOneHands = readRawHandsCollection(rawHands.first);
    Card::HandsCollection playerTwoHands = readRawHandsCollection(rawHands.second);

    return countWins(playerOneHands, playerTwoHands);
}

