#include "s2_Card.h"

namespace s2 {

Card::Card() : cardValue(0), suit('N') {
}

Card::Card(const char& value, const char& suit) : cardValue(calculateValue(value)), suit(suit) {
}

/**
 * return the value of the card
 * @param value
 * @return
 */
int Card::calculateValue(const char& value) {

    switch (value) {
        case 'A':
            return cardValues::ace;
        case 'K':
            return cardValues::king;
        case 'Q' :
            return cardValues::queen;
        case 'J':
            return cardValues::jack;
        case 'T':
            return cardValues::ten;
        default:
            return value - '0';;
    }

}


}
