#include <iostream>
#include <fstream>
#include <sstream>

#include "s2_matcher.h"

namespace s2 {

inline std::pair<Card::HandsCollection, Card::HandsCollection> readStringsFromFile(const std::string& filePath) {

    Card::HandsCollection playerOneHands;
    Card::HandsCollection playerTwoHands;

    std::vector<Card> bufferOne;
    std::vector<Card> bufferTwo;

    std::ifstream inStream(filePath);
    std::string line;


    while (std::getline(inStream, line)) {
        std::istringstream buf(line);
        std::istream_iterator<std::string> beg(buf), end;
        std::vector<std::string> tokens(beg, end);
        auto handSize = 5;
        std::vector<std::string> playerOne(tokens.begin(), tokens.begin() + handSize);
        std::vector<std::string> playerTwo(tokens.begin() + handSize, tokens.end());

        for (int i = 0; i < handSize; i++) {
            bufferOne.emplace_back(Card(playerOne[i].front(), playerOne[i].back()));
            bufferTwo.emplace_back(Card(playerTwo[i].front(), playerTwo[i].back()));
        }

        playerOneHands.push_back(bufferOne);
        playerTwoHands.push_back(bufferTwo);

        bufferOne.clear();
        bufferTwo.clear();
    }

    return std::pair<Card::HandsCollection, Card::HandsCollection>(playerOneHands, playerTwoHands);
}

int salvatore_2() {
    std::pair<Card::HandsCollection, Card::HandsCollection> rawHands = readStringsFromFile("poker.txt");
    return countWins(rawHands.first, rawHands.second);
}

}

