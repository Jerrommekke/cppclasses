//
// Created by Jamahl Watson on 14/12/2017.
//

#ifndef EULER54_CARD_H
#define EULER54_CARD_H

#include <string>

namespace Euler54
{
    class Card_JW {
    public:
        Card_JW();
        explicit Card_JW(std::string id);

        bool operator==(const Card_JW &other) const;
        bool operator!=(const Card_JW &other) const;
        bool operator<(const Card_JW &other) const;
        bool operator>(const Card_JW &other) const;
        bool operator<=(const Card_JW &other) const;
        bool operator>=(const Card_JW &other) const;

        int operator+ (const Card_JW &other) const;
        int operator- (const Card_JW &other) const;

        Card_JW& operator++ ();
        Card_JW operator++ (int);

        Card_JW& operator-- ();
        Card_JW operator-- (int);

        friend std::ostream& operator<<(std::ostream& out, const Card_JW& card);
        friend std::istream& operator>>(std::istream& in, Card_JW& card);

        char getSuit() const;
        int getValue() const;

        bool isAdjacent(const Card_JW& other) const;
    private:
        char suit_;
        int value_;
    };
}



#endif //EULER54_CARD_H
