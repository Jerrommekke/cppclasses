//
// Created by Jamahl Watson on 14/12/2017.
//

#include "Card_JW.h"

#include <iostream>
#include <sstream>

namespace Euler54
{
    inline int getCardValue(const char& value) {
        switch (value) {
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case 'T':
                return 10;
            case 'J':
                return 11;
            case 'Q':
                return 12;
            case 'K':
                return 13;
            case 'A':
                return 14;
            default:
                throw std::invalid_argument("Error: Value not in range.");
        }
    }

    Card_JW::Card_JW() : value_(-1), suit_('a') {};

    Card_JW::Card_JW(std::string id) :
        value_(getCardValue(id.front())),
    suit_(id.back()){}

    bool Card_JW::operator==(const Card_JW &other) const {
        return this->value_ == other.value_;
    }

    bool Card_JW::operator!=(const Card_JW &other) const {
        return this->value_ != other.value_;
    }

    bool Card_JW::operator<(const Card_JW &other) const
    {
        return this->value_ < other.value_;
    }

    bool Card_JW::operator>(const Card_JW &other) const
    {
        return this->value_ > other.value_;
    }

    bool Card_JW::operator<=(const Card_JW &other) const
    {
        return this->value_ <= other.value_;
    }

    bool Card_JW::operator>=(const Card_JW &other) const
    {
        return this->value_ >= other.value_;
    }

    std::ostream& operator<<(std::ostream& out, const Card_JW& card)
    {
        out << card.value_ << card.suit_;
        return out;
    }

    std::istream& operator>>(std::istream& in, Card_JW& card)
    {
        std::string tmp;
        in >> tmp;

        card.value_ = getCardValue(tmp.front());
        card.suit_ = tmp.back();

        return in;
    }

    char Card_JW::getSuit() const
    {
        return suit_;
    }

    int Card_JW::getValue() const
    {
        return value_;
    }

    int Card_JW::operator+(const Card_JW &other) const
    {

        return value_ + other.value_;
    }

    int Card_JW::operator-(const Card_JW &other) const
    {

        return value_ - other.value_;
    }

    Card_JW& Card_JW::operator++ ()
    {
        value_++;
        if ( value_ > 14 )
            value_ = 2;
        return *this;
    }

    Card_JW Card_JW::operator++ (int)
    {
        Card_JW result(*this);
        ++(*this);
        return result;
    }

    Card_JW& Card_JW::operator-- ()
    {
        value_--;
        if ( value_ < 2 )
            value_ = 14;
        return *this;
    }

    Card_JW Card_JW::operator-- (int)
    {
        Card_JW result(*this);
        --(*this);
        return result;
    }

    bool Card_JW::isAdjacent(const Card_JW& other) const
    {
        return ( ++Card_JW(*this) == other || --Card_JW(*this) == other );
    }
}

