#ifndef POKER_GAME_CARD_H
#define POKER_GAME_CARD_H


#include <string>

class Card;

class Card {
public:
    enum cardValues{
        two = 2,
        three,
        four,
        five,
        six,
        seven,
        eight,
        nine,
        ten,
        jack,
        queen,
        king,
        ace
    };
    // Helper aliases for cards collections
    using HandsRawCollection = std::vector<std::vector<std::string>>;
    using Hand = std::vector<Card>;
    using HandsCollection = std::vector<Hand>;
    explicit Card();
    explicit Card(const char& value, const char& suit);
    const int cardValue;
    const char suit;
private:
    int calculateValue(const char& value);
};


#endif //POKER_GAME_CARD_H
