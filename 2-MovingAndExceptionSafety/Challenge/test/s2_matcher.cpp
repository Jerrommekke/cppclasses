
#include <vector>
#include <iostream>
#include <array>
#include "s2_Card.h"
#include "s2_matcher.h"

namespace s2 {

inline std::vector<int> stackPlayerHand(const Card::Hand& hand);

inline HandScore parseSortedHand(const std::vector<int>& valueScale);

inline int solveTie(const HandScore& score, const std::vector<int>& playerOne, const std::vector<int>& playerTwo);

inline int checkHighestCard(const std::vector<int>& playerOne, const std::vector<int>& playerTwo);

inline int compareHands(const Card::Hand& firstPlayerHand, const Card::Hand& secondPlayerHand);

inline int checkPairTie(const std::vector<int>& playerOne, const std::vector<int>& playerTwo);

inline int checkDoublePairTie(const std::vector<int>& playerOne, const std::vector<int>& playerTwo);

inline int checkFullHouseTie(const std::vector<int>& playerOne, const std::vector<int>& playerTwo);

inline int checkThreeOfAKindTie(const std::vector<int>& playerOne, const std::vector<int>& playerTwo);

inline int checkFourOfAKindTie(const std::vector<int>& playerOne, const std::vector<int>& playerTwo);


/***
 *
 * @param playerOneHands
 * @param playerTwoHands
 * @return Number of times player one has won
 */
long long countWins(const Card::HandsCollection& playerOneHands,
                    const Card::HandsCollection& playerTwoHands) {
    long long playerOneWins = 0ll;

    for (int i = 0; i < playerOneHands.size(); i++) {
        if (compareHands(playerOneHands[i], playerTwoHands[i]) > 0) {
            playerOneWins++;
        }
    }

    return playerOneWins;
}

inline int compareHands(const Card::Hand& firstPlayerHand, const Card::Hand& secondPlayerHand) {

    std::vector<int> playerOneStacks = stackPlayerHand(firstPlayerHand);
    HandScore firstScore = parseSortedHand(playerOneStacks);
    std::vector<int> playerTwoStacks = stackPlayerHand(secondPlayerHand);
    HandScore secondScore = parseSortedHand(playerTwoStacks);

    if (firstScore == secondScore) {
        return solveTie(firstScore, playerOneStacks, playerTwoStacks);
    }

    if (firstScore > secondScore) {
        return PLAYER_ONE_WIN;
    }

    return PLAYER_TWO_WIN;
}

/**
 * We put the values on the hand in an array in which every position is the card value
 * and the first element is an int > 0 if the cards are all of the same suit
 * This way is much easier to analyze our hand
 * @param hand
 * @return
 */
inline std::vector<int> stackPlayerHand(const Card::Hand& hand) {
    std::vector<int> valueScale(15);

    bool sameSuit = true;
    const char firstSuit = hand.front().suit;

    for (auto& card : hand) {
        valueScale[card.cardValue]++;
        if (card.suit != firstSuit) {
            sameSuit = false;
        }
    }

    valueScale[0] = sameSuit;
    return valueScale;
};

/***
 * Gets an hand and return its score
 * @param valueScale
 * @return
 */
inline HandScore parseSortedHand(const std::vector<int>& valueScale) {

    bool sameSuit = valueScale[0] > 0;

    bool hasHighFlush = valueScale[Card::cardValues::ace] && valueScale[Card::cardValues::king]
                        && valueScale[Card::cardValues::queen] && valueScale[Card::cardValues::jack]
                        && valueScale[Card::cardValues::ten];

    if (sameSuit && hasHighFlush) {
        return HandScore::royalFlush;
    }

    bool hasStraight = false;
    int consecutiveCards = 0;

    for (auto index = valueScale.rbegin();
         index != valueScale.rend(); ++index) {
        if (consecutiveCards == 5) {
            hasStraight = true;
            break;
        } else if (*index > 0) {
            consecutiveCards++;
        } else {
            consecutiveCards = 0;
        }
    }

    if (hasStraight) {
        if (sameSuit) {
            return HandScore::straightFlush;
        } else {
            return HandScore::straight;
        }
    }

    if (std::find(std::begin(valueScale), std::end(valueScale), 4) != std::end(valueScale)) {
        return HandScore::fourOfAKind;
    }


    bool hasThreeOfAKind = std::find(std::begin(valueScale), std::end(valueScale), 3) != std::end(valueScale);
    long numPairs = std::count(std::begin(valueScale), std::end(valueScale), 2);

    if (hasThreeOfAKind && numPairs > 0) {
        return HandScore::fullHouse;
    }

    if (sameSuit) {
        return HandScore::flush;
    }

    if (hasThreeOfAKind) {
        return HandScore::threeOfAKind;
    }

    if (numPairs == 2) {
        return HandScore::twoPairs;
    }

    if (numPairs == 1) {
        return HandScore::onePair;
    }

    return HandScore::none;
}

inline int checkHighestCard(const std::vector<int>& playerOne, const std::vector<int>& playerTwo) {
    for (auto firstIndex = playerOne.rbegin(), secondIndex = playerTwo.rbegin();
         firstIndex != playerOne.rend() && secondIndex != playerTwo.rend(); ++firstIndex, ++secondIndex) {
        if (*firstIndex > *secondIndex) {
            return PLAYER_ONE_WIN;
        }
        if (*firstIndex < *secondIndex) {
            return PLAYER_TWO_WIN;
        }
    }
    // never reaches here -> there is always a clear winner
    return 0;
}

inline int solveTie(const HandScore& score, const std::vector<int>& playerOne, const std::vector<int>& playerTwo) {

    switch (score) {
        case onePair: {
            return checkPairTie(playerOne, playerTwo);
        }
        case twoPairs: {
            return checkDoublePairTie(playerOne, playerTwo);
        }
        case fullHouse: {
            return checkFullHouseTie(playerOne, playerTwo);
        }
        case threeOfAKind: {
            return checkThreeOfAKindTie(playerOne, playerTwo);
        }
        case fourOfAKind: {
            return checkFourOfAKindTie(playerOne, playerTwo);
        }
        default:
            return checkHighestCard(playerOne, playerTwo);
    }
}

inline int checkPairTie(const std::vector<int>& playerOne, const std::vector<int>& playerTwo) {
    auto firstRank = std::distance(std::begin(playerOne),
                                   std::find(std::begin(playerOne), std::end(playerOne), 2));
    auto secondRank = std::distance(std::begin(playerTwo),
                                    std::find(std::begin(playerTwo), std::end(playerTwo), 2));
    if (firstRank == secondRank) {
        return checkHighestCard(playerOne, playerTwo);
    }
    if (firstRank > secondRank) {
        return PLAYER_ONE_WIN;
    }
    return PLAYER_TWO_WIN;

}

inline int checkDoublePairTie(const std::vector<int>& playerOne, const std::vector<int>& playerTwo) {
    // check first couple
    auto firstRank = std::distance(playerOne.rbegin(),
                                   std::find(playerOne.rbegin(), playerOne.rend(), 2));
    auto secondRank = std::distance(playerTwo.rbegin(),
                                    std::find(playerTwo.rbegin(), playerTwo.rend(), 2));

    if (firstRank == secondRank) {
        // check second couple
        std::vector<int> restOfPlayerOne(playerOne.begin(), playerOne.end() - firstRank);
        std::vector<int> restOfPlayerTwo(playerTwo.begin(), playerTwo.end() - firstRank);
        return checkPairTie(restOfPlayerOne, restOfPlayerTwo);
    }
    if (firstRank < secondRank) {
        return PLAYER_ONE_WIN;
    }
    return PLAYER_TWO_WIN;
}

inline int checkFullHouseTie(const std::vector<int>& playerOne, const std::vector<int>& playerTwo) {
    // check highest tris
    auto firstRank = std::distance(std::begin(playerOne),
                                   std::find(std::begin(playerOne), std::end(playerOne), 3));
    auto secondRank = std::distance(std::begin(playerTwo),
                                    std::find(std::begin(playerTwo), std::end(playerTwo), 3));
    if (firstRank == secondRank) {
        return checkPairTie(playerOne, playerTwo);
    }
    if (firstRank > secondRank) {
        return PLAYER_ONE_WIN;
    }
    return PLAYER_TWO_WIN;
}

inline int checkThreeOfAKindTie(const std::vector<int>& playerOne, const std::vector<int>& playerTwo) {
    auto firstRank = std::distance(std::begin(playerOne),
                                   std::find(std::begin(playerOne), std::end(playerOne), 3));
    auto secondRank = std::distance(std::begin(playerTwo),
                                    std::find(std::begin(playerTwo), std::end(playerTwo), 3));
    if (firstRank == secondRank) {
        return checkHighestCard(playerOne, playerTwo);
    }
    if (firstRank > secondRank) {
        return PLAYER_ONE_WIN;
    }
    return PLAYER_TWO_WIN;
}

inline int checkFourOfAKindTie(const std::vector<int>& playerOne, const std::vector<int>& playerTwo) {
    auto firstRank = std::distance(std::begin(playerOne),
                                   std::find(std::begin(playerOne), std::end(playerOne), 4));
    auto secondRank = std::distance(std::begin(playerTwo),
                                    std::find(std::begin(playerTwo), std::end(playerTwo), 4));
    if (firstRank == secondRank) {
        return checkHighestCard(playerOne, playerTwo);
    }
    if (firstRank > secondRank) {
        return PLAYER_ONE_WIN;
    }
    return PLAYER_TWO_WIN;
}

} 

