
#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;




template<class T1, class T2>
T1 ToThePower(T1 a, T2 power)
{
    return std::pow(a, power);
}

template<class T1>
T1 ToThePower<T1, int>(T1 a, int power)
{
    int b = a;
    for (int i = 1; i < power; ++i)
        b *= a;
    cout << "Using specialized function: ";
    return b;
}


int main()
{
    double pi = std::acos(-1.0);
    int    a = 3;
    cout << std::setprecision(3) << pi << " ^ " << 2 << " = " << ToThePower(pi, 2) << "\n";
    cout << a << " ^ " << 3 << " = " << ToThePower(a, 3) << "\n";

}













