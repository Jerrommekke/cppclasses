
#include <iostream>
#include <vector>

using namespace std;


template<class T>
class IsPointer
{
public:
    enum { value = 0};
};

// Template specialization
template<class T>
class IsPointer<T*>
{
public:
    enum { value = 1};
};


int main()
{
 
    std::vector<int> p = { 1 , 3, 4};

    cout << "Double is a " << ( IsPointer<double>::value ? "pointer" : "value type" ) << "\n";
    cout << "Double* is a " << ( IsPointer<double*>::value ? "pointer" : "value type" ) << "\n";
    cout << "Double** is a " <<  IsPointer<double**>::value  << "\n";
    cout << "vector<int> iterator is a " << ( IsPointer< std::vector<int>::iterator >::value ? "pointer" : "value type" ) << "\n";

}