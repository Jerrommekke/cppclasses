
#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip>
#include <sstream>

using namespace std;


template<class accum_t, class item_t>
class Accumulator
{
public:
    Accumulator(accum_t init) : 
        accum_(init) {}

    void operator()(const item_t& a)
    {
        accum_ += (accum_t)a;
    }

    accum_t result() const { return accum_; }
private:
    accum_t accum_; 
};




int main()
{
    // standard accumulator
    std::vector<int> a = { 5 , 2 , 4, 8, 9 , 10 };
    
    Accumulator<long long, int> accum(0);
    for (const auto& elem : a)
        accum(elem);

    cout << "Accumulated result: " << accum.result() << "\n"; 


    //String accumulator 
    std::vector<string> b = { "Hello, ", "world!" };
    Accumulator<string, string> string_accum(""s);
    for (const auto& elem: b)
        string_accum(elem);

    cout << "String accumulatorion: " << string_accum.result() << "\n";

    /*

    // Stringify
    Accumulator<string, int> stringify_accum;
    for (const auto& elem: a)
        stringify_accum(elem);
    
    cout << "Stringified: " << stringify_accum.result() << "\n";


    // vector accumulator
    std::vector< std::vector<int> > c = { { 2, 5 }, { 1, 4}, { 6, 7} };
    Accumulator<long long, std::vector<int> > vector_accum(0ll);
    for (const auto& elem : c)
        vector_accum(elem);

    cout << "Vector accumulatorion: " << vector_accum.result() << "\n";

    */

}






template<class item_t>
class Accumulator<string, item_t>
{
public:
    Accumulator(string init = string()) 
    {
        if (!init.empty())
            ss_ << init << ",";
    }

    void operator()(const item_t& a)
    {
        ss_ << a << ",";
    }

    string result() const { return ss_.str(); }
private:
    stringstream ss_;
};



template<class accum_t, class item_t> 
class Accumulator< accum_t, vector<item_t> >
{
public:
   Accumulator(accum_t init) : 
        accum_(init) {}

    void operator()(const vector<item_t>& a)
    {
        for (const auto& elem : a)
            accum_ += elem;
    }

    accum_t result() const { return accum_; }
private:
    accum_t accum_;  

};







