#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <list>
#include <vector>
#include <numeric>

using namespace std;


// Read contents of text file and return as string
string getFileContents(string filename)
{
    ifstream ifs(filename);
    stringstream stringBuffer;
    stringBuffer << ifs.rdbuf();
    return stringBuffer.str();
}

// Split string into sections
vector<string> split(string source)
{
    vector<string> result;
    stringstream ss(source);
    for (string name; getline(ss, name, ','); )
        result.push_back( name );
    return result;
}



int main()
{
    string contents = getFileContents("names.txt");
    vector<string> names = split(contents);
    sort(names.begin(), names.end()); 
  
    long long total { 0ll };
    long long iter { 0ll };
    for (const string& name : names)
    {
      long long score { 0ll };
      for (const char& c : name.substr(1, name.size()-2) ) // account for quotes in substr
        score += c - 'A' + 1ll;
      total += score*(++iter);
    }
    cout << "Total score: " << total << "\n"; 
}
