
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <cmath>

using namespace std;


class VerySpecialInt
{
public:
    explicit VerySpecialInt(int value) : 
        value_(value) {}

    string toString() const 
    {
        stringstream strm;
        strm << "int: " << value_ << "!!!!!";
        return strm.str();
    }

    VerySpecialInt& operator+=(const VerySpecialInt& other)
    {
        value_ += other.value_;
        return *this;
    }
    
private:
    int value_; 
};


class VerySpecialDouble
{
public:
    explicit VerySpecialDouble(double value) : 
        value_(value) {}

    string toString() const 
    {
        stringstream strm;
        strm << "double: " <<  std::setprecision(3) << value_ << "!!!!!";
        return strm.str();
    }

    VerySpecialDouble& operator+=(const VerySpecialDouble& other)
    {
        value_ += other.value_;
        return *this;
    }
    
private:
    double value_; 
};


template<typename T>
void addTwoAndPrint(T i)
{
    i += T(2);
    std::cout << "addTwoAndPrint -> " << i.toString() << "\n";
}


int main()
{

    VerySpecialInt i { 42 } ;
    VerySpecialDouble d { std::acos(-1L)};
    

    cout << "Hello, world" << "\n";
    cout << "d = " << d.toString() << "\n";
    cout << "i = " << i.toString() << "\n";

    int a = 5;
    addTwoAndPrint(a);

}