#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <list>
#include <vector>
#include <deque>
#include <numeric>
#include <array>

using namespace std;

// Split string into sections
template<class OutputIterator, typename T>
void split(std::basic_istream<T>& sourcestream, T delim, OutputIterator c)
{
    for (string name; getline(sourcestream, name, delim); )
        *c++ = name; 
}


int main()
{
    deque<int> tmp; 
    deque<string> names; // = { "init name "} ;

    ifstream strm("names.txt");
    split(strm, ',', back_inserter(tmp) );
    sort(names.begin(), names.end());
  
    long long total { 0ll };
    long long iter { 0ll };
    for (const string& name : names)
    {
      long long score { 0ll };
      for (const char& c : name.substr(1, name.size()-2) ) // account for quotes in substr
        score += c - 'A' + 1ll;
      total += score*(++iter);
    }
    cout << "Total score: " << total << "\n"; 
}
