#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <sstream>


using namespace std;


// Homework Question 2a:
// Filter the input, which is a collection of a collection of some type or class (i.e. nested collection)
// so that the all elements of the output are symmetric, i.e. (simplified syntax, syntax is open to define by implementer)
// SymmetricOnly({ "YAY", "Hello", "World"})  // Should return {"YAY"}
// SymmetricOnly({ { 1 }, { 1, 2 }, { 5, 2, 1}, { 1, 1} ) // Should return { {1}, {1, 1} }
// Non primitive nested collections must have the operator != implemented (or the operator==, depending on your algorithm)
// BONUS:
// Template variants: Implement the function in such a way that it can do an in-place or a copy-filter, depending on input parameters
//
// Homework Question 2b:
// Allow the same function (at least from the caller's perspective, i.e. the same function syntax) to also work on collections 
// of primitive integer types (i.e. int) (BONUS: can you include long long, unsigned int, etc). 
// SymmetricOnly({ 1, 12, 11, 215, 404}) // should return { 1, 11, 404 }
// BONUS 1:
// Template safety: Have the compiler throw an error for double and float inputs (because the unprecise nature of these types would not allow
// a reliable check on symmetry)
// BONUS 2:
// Template classes: Can you solve questions 2a and/or 2b using template classes instead of a template method (or, if you've initially used classes,
// now try to solve it using template method(s)) (NOTE: in C++14 template classes cannot auto-induce template arguments )



template<typename T>
bool is_symmetric(const T& elem) 
{
    auto riter = elem.rbegin(); 
    auto iter = elem.begin();
    for (int i = 0; i < elem.size()/2; ++i, ++iter, ++riter)
    {
        if (*iter != *riter)
            return false;
    }
    return true;
}

bool is_symmetric(const int& i)
{
    stringstream s;
    s << i;
    return is_symmetric(s.str());
}

template<class T>
class SymmetryAnalyzer
{
public:
    SymmetryAnalyzer(T value) { 
        stringstream s;
        s << value;
        string_ = s.str();
    }

    string::reverse_iterator rbegin() { return string_.rbegin(); }
    string::iterator begin() { return string_.begin(); }

    string::const_reverse_iterator rbegin() const { return string_.rbegin(); }
    string::const_iterator begin() const { return string_.begin(); }

private:
    string string_;

};


template<class InputIterator, class OutputIterator>
OutputIterator SymmetricOnly(InputIterator begin, InputIterator end, OutputIterator out)
{
    return std::remove_copy_if(begin, end, out, [](const auto& elem) { 
        return !is_symmetric(elem);
    });
}

template<class A, class B >
void checkSolution(A solution, B correct_solution)
{
    for (const auto& l : correct_solution)
    {
        auto iter = find(solution.begin(), solution.end(), l);
        if (iter == solution.end())
        {
            cout << "ERROR: invalid solution, missing element \n";
            return;
        }
        solution.erase(iter);
    }
    if (solution.size() > 0)
        cout << "ERROR: invalid solution, unexpected element \n";
}


int main()
{
    // Question 2a. 

    //** vector<string>
    
    std::vector< string> strings = { "YAY", "Hello", "World", "redivider", "wait", "cheap", "carriage", "demonic", "madam", "diligent" };
    cout << "vector<string>:: Before filtering: ";
    for (const auto& s : strings)
        cout << s << ", ";
    cout << "\n";

    // C++ STL syntax, input iterator begin, input iterator end, output iterator. Feel free to modify 
    std::list<string> filtered_strings;
    SymmetricOnly(strings.begin(), strings.end(), back_inserter(filtered_strings));
    
    cout << "After filtering: ";
    for (const auto& s : filtered_strings)
        cout << s << ", ";
    cout << "\n";

    vector<string> string_solution = { "YAY", "redivider", "madam" };
    checkSolution(string_solution, filtered_strings);


    //*** list<list<int> > ***

    std::list< std::list<int> > lists = { { 1 }, { 1, 2 }, { 5, 2, 1}, { 1, 1}, { 4, 0, 4}, {2, 0, 5}, {4, 3, 6, 3}, {2, 3, 4, 2, 5, 6}, {6, 4, 3, 4, 6}};

    cout << "list<list<int>>:: Before filtering: ";
    for (const auto& l : lists) 
    {
        cout << "{";
        for (const auto & i : l) {
            cout << i << ", ";
        }
        cout << "},";
    }
    cout << "\n";

    std::list< std::list<int> > filtered_lists; 
    SymmetricOnly(lists.begin(), lists.end(), back_inserter(filtered_lists));
    
    cout << "After filtering: ";
    for (const auto& l : filtered_lists) 
    {
        cout << "{";
        for (const auto & i : l) {
            cout << i << ", ";
        }
        cout << "},";
    }
    cout << "\n";

    list< list<int> > list_solution = { {1}, { 1, 1}, {4, 0, 4}, {6, 4, 3, 4, 6} };
    checkSolution(list_solution, filtered_lists);


    // Question 2b.
    // Uncomment when you're ready for it
    //** list<int>

    std::list< int > ints = { 1, 12, 521, 11, 404, 205, 4363, 234256, 64346 };
    cout << "list<int> Before filtering: ";
    for (const auto& i : ints)
        cout << i << ", ";
    cout << "\n";

    std::list<int> filtered_ints;
    SymmetricOnly(ints.begin(), ints.end(), back_inserter(filtered_ints));
    
    cout << "After filtering: ";
    for (const auto& i : filtered_ints)
        cout << i << ", ";
    cout << "\n";
    
    list<int> int_solution = { 1, 11, 404, 64346 };
    checkSolution(int_solution, filtered_ints);

}