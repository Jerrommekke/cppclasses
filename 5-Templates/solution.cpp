#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <list>
#include <vector>
#include <numeric>

using namespace std;
int main()
{
  ifstream ifs("names.txt");
  vector<string> names;
  for (string name; getline(ifs, name, ','); )
    names.push_back( name.substr(1, name.size() -2 ) );
  sort(names.begin(), names.end());
  
  long long total { 0ll };
  long long iter { 0ll };
  for (const string& name : names)
  {
    long long score { 0ll };
    for (const char& c : name)
      score += c - 'A' + 1ll;

    score *= (++iter);
    total += score;
  }
  cout << "Total score: " << total << "\n"; 
}
