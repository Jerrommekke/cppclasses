#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <list>
#include <vector>
#include <deque>
#include <numeric>
#include <array>

using namespace std;

// Split string into sections
vector<string> split(std::basic_istream<char>& sourcestream, char delim)
{
    vector<string> result;
    for (string name; getline(sourcestream, name, delim ); )
        result.push_back(name); 
      return result;
}

int main()
{
    ifstream strm("names.txt");
    deque<string> names = split(strm, ',');
    sort(names.begin(), names.end());
  
    long long total { 0ll };
    long long iter { 0ll };
    for (const string& name : names)
    {
      long long score { 0ll };
      for (const char& c : name.substr(1, name.size()-2) ) // account for quotes in substr
        score += c - 'A' + 1ll;
      total += score*(++iter);
    }
    cout << "Total score: " << total << "\n"; 
}
