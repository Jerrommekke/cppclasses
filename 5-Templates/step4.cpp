#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <list>
#include <vector>
#include <deque>
#include <numeric>
#include <array>

using namespace std;

// Split string into sections
template<class OutputIterator,
    bool IsOutputIterator =  OutputIterator == typename std::enable_if<std::is_convertible<typename
                                       std::iterator_traits<OutputIterator>::iterator_category,
                                       std::output_iterator_tag>::value >::type> 
void split(std::basic_istream<char>& sourcestream, char delim, OutputIterator c)
{
    if (IsOutputIterator)
    {
        for (string name; getline(sourcestream, name, delim); )
            *c++ = name; 
    }
    else {
        cout << "OH NOES";
    }
}


int main()
{
    deque<string> names;

    ifstream strm("names.txt");
    split(strm, ',', names.begin() );
    sort(names.begin(), names.end());
  
    long long total { 0ll };
    long long iter { 0ll };
    for (const string& name : names)
    {
      long long score { 0ll };
      for (const char& c : name.substr(1, name.size()-2) ) // account for quotes in substr
        score += c - 'A' + 1ll;
      total += score*(++iter);
    }
    cout << "Total score: " << total << "\n"; 
}
