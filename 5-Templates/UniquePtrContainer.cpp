#include <iostream>
#include <vector>
#include <memory>

using namespace std;


class Bar
{
public:
    virtual ~Bar() {}

    void sayHello() 
    {
        cout << "Hello!\n";
    }

    virtual void sayHi()
    {
        cout << "Bar says Hi\n";
    }

};

class Foo : public Bar
{
public:
    void sayHi()
    {
        cout << "Foo says Hi\n";
    }

};


int main()
{
    Foo f;
    Bar b;

    f.sayHi();
    b.sayHi();


    /*
    cout << "In vector: \n";
    vector< Bar> v;
    v.push_back(f);
    v.push_back(b);

    for (auto& b : v)
        b.sayHi();

    cout << "In ptr-vector: \n";
    vector< unique_ptr<Bar> > v2;

    // will not compile
    //std::unique_ptr<Foo> f2( new Foo() ) ;
    //v2.push_back( f2);

    v2.emplace_back(new Foo());
    v2.emplace_back(new Bar());

    for (auto& b : v2)
        b->sayHi();

    */

}