//  $Id$
//
//  (c) Intrasonics Limited, 2013.  All rights reserved.
//
//  This software is the property of Intrasonics Limited and may not be copied or reproduced
//  otherwise than on to a single hard disk for backup or archival purposes. The source code is
//  confidential information and must not be disclosed to third parties or used without the
//  express written permission of Intrasonics Limited.

#include "TimedEvent.h"
#include "Logger.h"

using namespace std;
using namespace std::chrono;

namespace intrasonics
{

TimedEvent::TimedEvent(function<void()> eventFunction)
:
    mWaitLock(),
    mWaitCondition(),
    mDuration(),
    mEventFunction(eventFunction),
    mCancelled(false),
    mRunning(false),
    mReset(false),
    mDone(false),
	mThread([=]()
			{

				unique_lock<mutex> waitLock(mWaitLock);

				// Wait until doEventAfter() called, then sleep for the right time,
				// then do the event and repeat until destroyed.
				while(!mDone)
				{
					mRunning = false;

					if(!mReset)
						mWaitCondition.wait(waitLock);  // Waiting for something to start the timer, so drop through if we have been reset
					else
					{
						mReset = false;
						mCancelled = false;
					}

					mRunning = true;

					if(!mCancelled)
					{
						mWaitCondition.wait_for(waitLock, mDuration);

						try
						{
							if(!mCancelled)
								mEventFunction();
							else
								mCancelled = false;
						}
                        catch (exception e)
                        {
                            Logger::Warning(nullptr, "TimedEvent thread died with an exception: ", e.what());
                        }
						catch(...)
						{
							// RI-75
                            Logger::Warning(nullptr, "TimedEvent thread died with an unknown exception");
						}
					}
				}
			})
{
}


void TimedEvent::DoEventAfter(milliseconds duration)
{
	{
		lock_guard<mutex> lock(mWaitLock);

		if(mRunning)
		{
			mCancelled = true;
			mReset = true;
		}
		else
		{
			mCancelled = false;
			mReset = false;
		}

		mDuration = duration;
	}

	mWaitCondition.notify_one();
}

void TimedEvent::Cancel()
{
	mCancelled = true;
	mWaitCondition.notify_one();
}

TimedEvent::~TimedEvent()
{
    Logger::Debug(nullptr, "TimedEvent: destructor entry");
    mDone = true;
	Cancel();
	mThread.join();
    Logger::Debug(nullptr, "TimedEvent: destructor exit");
}


} // namespace intrasonics
