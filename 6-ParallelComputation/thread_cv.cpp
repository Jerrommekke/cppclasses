#include <thread>
#include <iostream>
#include <chrono>
#include <condition_variable>

using namespace std;

mutex iostream_mutex;
mutex ready_mutex;
condition_variable cv;


void countDown(const std::string& name, bool& ready, condition_variable& condition)
{

  for (int i = 3; i >= 0; --i) { 
    
    {
      lock_guard<mutex> lock(iostream_mutex);

      cout << name << ": " << i << endl;

      if (i == 1)
      {
          {
            // Modify ready under a lock
            lock_guard<mutex> l2(ready_mutex);
            ready = true; 
          }
          // notify the world
          condition.notify_one();
      }
    }

    this_thread::sleep_for(chrono::seconds(1));
  }

}


int main()
{

  bool ready = false;

  thread thread1( &countDown, "Thread 1", ref(ready), ref(cv)  );
  
  {
    unique_lock<mutex> readylock(ready_mutex);
    while (!ready)
      cv.wait(readylock);
  }

  thread thread2( &countDown, "Thread 2", ref(ready), ref(cv)  );

  thread1.join();
  thread2.join();


}
