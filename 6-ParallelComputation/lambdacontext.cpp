#include <iostream>

#include <functional>


using namespace std;


void doSomething()
{
  cout << "I'm doing something!" << std::endl;
}

int add(int a, int b)
{
  return a+b;
}


int main()
{

  //The function pointer, 'the old' C style
  void (*oldCPtr)() = &doSomething;
  int (*ptrToAdd)(int,int) = &add;
  oldCPtr();

  cout << "The sum of 2 + 3 = " << ptrToAdd(2,3) << std::endl;

  // Nicer usage, but horrible typedef
  typedef void (*myFunctionType)();
  myFunctionType typedCPtr = &doSomething;
  typedCPtr();


  // C++11 awesomeness
  auto cppPtr = &doSomething;
  auto cppAdder = &add;
  cout << "C++: ";
  cppPtr();
  cout << "The sum of 2 + 3 = " << cppAdder(2,3) << std::endl;

  

  // C++ functors
  function<void()> cppFunctor = &doSomething;
  function<int(int,int)> functorAdd = &add;
  double a = 5;
  cout << "Functor: ";
  cppFunctor();
  cout << "Functor 2 + 3: " << functorAdd(2,3) << std::endl;


  {
      // Remember Nick's talk? 
      using namespace std::placeholders;
      auto add2 = bind( &add, _3, _1);
      cout << "Now using bind: 2+3 = " << add2(3,1231254,2) << endl;
  }

  
  // Lambda's
  function<int(string)> lambda = [](string name) { cout << "Look, " << name << ", at how modern my code is!" << endl; return 5; }; 
  lambda("Pete");

  

  int variable = 4;
  int variable2 = 4;

  auto printVariableValue = [&variable, variable2](){ 
    cout << "variable: " << variable << ", 2: " << variable2 << std::endl;
  };

  variable++;
  variable2++;

  printVariableValue();
  exit(0);
      

}

