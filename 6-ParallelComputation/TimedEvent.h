//  $Id$
//
//  (c) Intrasonics Limited, 2013.  All rights reserved.
//
//  This software is the property of Intrasonics Limited and may not be copied or reproduced
//  otherwise than on to a single hard disk for backup or archival purposes. The source code is
//  confidential information and must not be disclosed to third parties or used without the
//  express written permission of Intrasonics Limited.

#ifndef TIMEDEVENT_H_
#define TIMEDEVENT_H_

#include <thread>
#include <functional>
#include <chrono>
#include <mutex>
#include <condition_variable>

namespace intrasonics
{

class TimedEvent
{
public:
	TimedEvent(std::function<void()> eventFunction);
	virtual ~TimedEvent();

	void DoEventAfter(std::chrono::milliseconds duration);
	void Cancel();
    
	inline bool isCancelled() const { return mCancelled; }

private:
	std::mutex mWaitLock;
	std::condition_variable mWaitCondition;
    std::chrono::milliseconds mDuration;
	std::function<void()> mEventFunction;

	bool mCancelled;
	bool mRunning;
	bool mReset;
    bool mDone;
    
    std::thread mThread;
};

} // namespace intrasonics
#endif // TIMEDEVENT_H_
