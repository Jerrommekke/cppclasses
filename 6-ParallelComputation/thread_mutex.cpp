#include <thread>
#include <iostream>
#include <chrono>
#include <mutex>

using namespace std;



void countDown(const std::string& name, mutex& iostream_mutex)
{
  for (int i = 3; i >= 0; --i) { 
    
    {
      lock_guard<mutex> lock(iostream_mutex);
      cout << name << ": " << i << endl;
    }
    this_thread::sleep_for(chrono::seconds(1));
  }

}


int main()
{

  mutex iostream_mutex;

  thread thread1( &countDown, "Thread 1", ref(iostream_mutex) );
  thread thread2( &countDown, "Thread 2", ref(iostream_mutex) );
  
  thread1.join();
  thread2.join();

}
