#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <cmath>

#ifndef SYSOUT_F
#define SYSOUT_F(f, ...)      _RPT1( 0, f, __VA_ARGS__ ) // For Visual studio
#endif

#ifndef speedtest__
#define speedtest__(data)   for (long blockTime = NULL; (blockTime == NULL ? (blockTime = clock()) != NULL : false); SYSOUT_F(data "%.9fs", (double) (clock() - blockTime) / CLOCKS_PER_SEC))
#endif

using namespace std;


template<typename T>
vector<T> readdata(const string& filename, typename vector<T>::size_type size_hint = 0)
{
    vector<T> data;
    if (size_hint > 0) data.reserve(size_hint);

    ifstream strm(filename);
    if (!strm.is_open())
        throw runtime_error("I'm too lazy to deal with nonexistent files");

    for (string word; getline(strm, word, ','); )
        data.push_back( atof(word.c_str()) );

    return data;
}


template<typename T>
void multiply(const vector<T>& a, const vector<T>& b, vector<T>& c)
{
    auto it3 = c.begin();
    for ( auto it1 = a.begin(), it2 = b.begin(); it1 != a.end(); it1++, it2++, it3++ )
        *it3 = *it1 * *it2;
}

template<typename T>
T sum(const vector<T>& a)
{
    T result = 0;
    for (const auto& elem : a)
        result += elem;
    /* I'm avoiding accumulate to annoy Nick :-) */
    return result;
}

template<typename T>
void matrix_multiply( T **a, T **b, T **c, int n)
{
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            c[i][j] = 0;
            for (int k = 0; k < n; ++k)
                c[i][j] += a[i][k]*b[k][j];
        }
    }
}



int main(int argc, char *argv[])
{
    // Read requested number of threads from command line arguments, i.e. --threads 6
    int numThreads = 1;
    for (int i = 0; i < argc -1; ++i)
        if ( strcmp(argv[i], "--threads") == 0  )
            numThreads = atoi(argv[i+1]);

    if (numThreads == 0) numThreads = 1;

    cout << "Parallel computation requested in " << numThreads << " threads.\n";
    cout << "Reading data...";
    cout.flush();

    typedef float precision_t;

    // Read data
    auto a = readdata<precision_t>("arr1.txt", 1000000);
    auto b = readdata<precision_t>("arr2.txt", 1000000);

    cout << "done" << endl;

    cout << "Enlarging arrays...";
    cout.flush();
    // Let's make a bigger to make this actually interesting ( and not spend too long reading data)
    int factor = 10;
    auto origSize = a.size();
    a.resize(a.size()*factor);
    b.resize(b.size()*factor);
    for (int i = 1; i < factor; ++i) {
        memcpy(a.data() + i*origSize, a.data(), sizeof(precision_t)*origSize);
        memcpy(b.data() + i*origSize, b.data(), sizeof(precision_t)*origSize);
    }
    vector<precision_t> result(a.size());
    cout << "done" << endl;
    

    // multiply data
    cout << "Multiplication of two arrays, size " << a.size() << ", in a single thread: " << endl;
    clock_t tStart = clock();
    multiply(a,b,result);
    auto singleThreadedMultiplicationTime = (double)(clock() - tStart)/CLOCKS_PER_SEC;
    cout << "Time taken: " << singleThreadedMultiplicationTime << "sec." << endl;

    cout << "Sum of array of size " << a.size() << " in a single thread: " << endl;
    tStart = clock();
    auto sumOfA = sum(a);
    auto singleThreadedReduceTime = (double)(clock() - tStart)/CLOCKS_PER_SEC;
    cout << "Result: " << sumOfA << ", time taken: " << singleThreadedReduceTime << "sec." << endl;

    // Now let's do a matrix multiplication
    // we'll use the same arrays, but now as 2d data
    int n = 1000;
    if (n > sqrt(a.size())) n = sqrt(a.size());
    cout << "Multiplication of two square arrays of size " << n << "x" << n << "." << endl;
    precision_t *mat1[n];
    precision_t *mat2[n];
    precision_t *mat3[n];

    precision_t *ptr1 = a.data();
    precision_t *ptr2 = b.data();
    precision_t *ptr3 = result.data();
    for (int i = 0; i < n; ++i, ptr1 += n, ptr2 += n, ptr3 += n)
    {
        mat1[i] = ptr1;
        mat2[i] = ptr2;
        mat3[i] = ptr3;
    }


    // multiply data
    tStart = clock();
    matrix_multiply(mat1, mat2, mat3, n);
    auto singleThreadedMatrixMultiplicationTime = (double)(clock() - tStart)/CLOCKS_PER_SEC;
    cout << "Time taken: " << singleThreadedMatrixMultiplicationTime << "sec." << endl;
}



