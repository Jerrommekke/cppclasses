

#include "say_hello.h"

// This defines the main function
int main()
{
    // The compiler now knows that sayHello() is a function
    // of the shape as declared above. That means the code below is legal:
    sayHello();
    return 0;
}