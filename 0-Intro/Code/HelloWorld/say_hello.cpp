//
// Created by Jerome Schalkwijk on 26/11/2017.
//

#include <string>
#include <iostream>


void sayHello()
{
    // This line declares the existence of a variable
    std::string textToPrint = "Hello, world!";
    // The following line uses the variable
    std::cout << textToPrint << std::endl;
}