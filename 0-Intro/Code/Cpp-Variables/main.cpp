#include <iostream>
#include <vector>


void addTwo(int elem)
{
    elem += 2;
}

void addTwoToList(std::vector<int> list)
{
    for (int i = 0; i < list.size(); ++i)
    {
        list[i] += 2;
    }

}


int main()
{

    int i = 5;
    std::vector<int> list; // note: DONT USE NEW!
    list.push_back(5);

    addTwo(i);
    addTwoToList(list);

    std::cout << "Value of i: " << i << std::endl;
    std::cout << "Values in list:" << std::endl;
    for (int value : list)
        std::cout << value << std::endl;




    return 0;
}




