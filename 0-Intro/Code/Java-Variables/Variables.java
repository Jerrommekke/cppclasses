import java.util.List;
import java.util.ArrayList;


public class Variables
{

    public static void main(String[] arguments)
    {
        doSomething();
    }

    public static void doSomething()
    {
        int element = 5;
        addTwo(element);
        //element is still 5

        System.out.println("Element value: " + element);

        List<Integer> values = new ArrayList<Integer>();
        values.add(5); 
        addTwoToAll(values);

        System.out.println("Values:");
        for (Integer i : values)
        {
            System.out.println(i); // what will it print?
        }
    }
        
    public static void addTwo(int value)
    {
        value = value + 2;
    }

    public static void addTwoToAll(List<Integer> values)
    {
        for (int i = 0; i < values.size(); i++)
        {
            values.set(i, values.get(i) + 2);
        }
    }


}




