#include <iostream>
#include <vector>

int main()
{

    // Now both of these are value types
    int i = 5;
    std::vector<int> list; // note: DONT USE NEW!
    list.push_back(5);


    // C++ has both "references", denoted by the ampersand (&)
    int& reference_to_i = i;
    std::vector<int>& reference_to_list = list;


    reference_to_i += 2;
    reference_to_list[0] += 2;

    // Now print
    std::cout << "Value of i: " << i << std::endl;
    std::cout << "Values in list:" << std::endl;
    for (int value : list)
        std::cout << value << std::endl;


    //Note that references can never be null - they must be declared and defined in one go:
    //int& reference_to_nothing; // this is illegal!


    // However, pointers can be null. Pointers can be thought of as nullable references
    int* pointer_to_i = &i;
    std::vector<int>* pointer_to_list = nullptr;
    pointer_to_list   = &list;

    // The ampersand (&) takes the "address" of an element, i.e. converts value into pointer

    // To modify pointers, we need to convert it back into a value, i.e. check that it is not null:
    if (pointer_to_i) // implicit nullcheck
    {
        int& ref = *pointer_to_i; // convert it back to reference
        ref = 9;

        std::cout << "Value of i after pointer modification: " << i << std::endl;

        // or, in short:
        *pointer_to_i = 10;

        std::cout << "Value of i after second pointer modification: " << i << std::endl;
    }



    // So we're always explicit: if we're not using either references or pointers, it's a value-type copy, i.e.
    std::vector<int> second_list = list;
    second_list[0] = 12;


    std::cout << "Values in list after second list modificatione:" << std::endl;
    for (int value : list)
        std::cout << value << std::endl;




    return 0;
}