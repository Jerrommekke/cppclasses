//
// Created by Jerome Schalkwijk on 30/11/2017.
//

#include "adjacent_multiplication.h"

class Buffer
{
public:
    Buffer(size_t size) :
            buffer_(size, 1), index_(size-1) {}

    long long add(int element)
    {
        index_++;
        if (index_ >= buffer_.size())
            index_ = 0;

        buffer_[index_] = element;

        long long product = 1ll;
        for (const auto& elem : buffer_)
            product *= elem;

        return product;
    }

    void clear()
    {
        for (auto& elem : buffer_)
            elem = 1;
    }

private:
    Row buffer_;
    Row::size_type index_;

};



long long getMaximumProduct(const Matrix& matrix)
{

    long long maxProd = 1ll;
    Buffer buffer(4);
    // maximum horizontally
    for (const auto &row : matrix)
    {
        buffer.clear();
        for (const int& elem : row)
            maxProd = std::max( buffer.add(elem), maxProd);
    }

    // maximum vertically
    for (auto col = 0u; col < matrix.front().size(); ++col)
    {
        buffer.clear();
        for (const auto &row : matrix)
            maxProd = std::max( buffer.add(row[col]), maxProd);
    }

    // maximum diagonally right
    for (int col = int(-matrix.front().size()); col < (int)matrix.front().size(); ++col)
    {
        buffer.clear();
        for (auto row = std::min(-col,0); row < matrix.size() && row+col < matrix[row].size(); ++row)
            maxProd = std::max( buffer.add(matrix[row][col+row]), maxProd);
    }

    // maximum diagonally left
    for (int col = 0; col < (int)matrix.front().size() + (int)matrix.size(); ++col)
    {
        buffer.clear();
        for (auto row = std::max(0, col - (int)matrix.front().size() ); row < matrix.size() && col-row >= 0; ++row)
            maxProd = std::max( buffer.add(matrix[row][col-row]), maxProd);
    }

    return maxProd;

}