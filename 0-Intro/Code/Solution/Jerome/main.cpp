#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "adjacent_multiplication.h"

inline Matrix getMatrix(const std::string& filename)
{
    Matrix matrix;

    std::ifstream fileSream(filename);
    for (std::string line; getline(fileSream, line); )
    {
        matrix.push_back( Row() );
        auto currentRow = matrix.end();
        currentRow--;

        std::istringstream inputStream(line);
        int value;
        while (inputStream >> value)
            currentRow->push_back(value);

    }
    return matrix;
}


int main()
{

    Matrix matrix = getMatrix("grid.txt");

    std::cout << "Hello, World!" << std::endl;
    std::cout << "Maximum product: " << getMaximumProduct(matrix) << std::endl;
    return 0;
}