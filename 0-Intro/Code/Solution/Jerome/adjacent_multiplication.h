//
// Created by Jerome Schalkwijk on 30/11/2017.
//

#ifndef SOLUTION_ADJACENTMULTIPLICATION_H
#define SOLUTION_ADJACENTMULTIPLICATION_H

#include <vector>
using Row    = std::vector<int>;
using Matrix = std::vector< Row >;


long long getMaximumProduct(const Matrix& matrix);



#endif //SOLUTION_ADJACENTMULTIPLICATION_H
