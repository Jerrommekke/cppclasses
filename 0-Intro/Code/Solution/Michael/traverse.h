#ifndef TRAVERSE_H
#define TRAVERSE_H
 
using Row    = std::vector<int>;
using Matrix = std::vector< Row >;

void compare(int highest,int contender);
int search(Matrix m);

#endif