#include <vector>
#include <iostream>
using Row = std::vector<int>;
using Matrix = std::vector< Row >;

int compare(int highest,int contender){
    if(contender > highest){
        return contender;
    }
    else{
        return highest;
    }
}

int search(Matrix m){
    int numOfRows = m.size();
    int numOfCols = m[0].size();
    int leftLim = 3;
    int rightLim = numOfCols - 3;
    int downLim = numOfRows - 3;
    int highest = 0;
    for(int y = 0;y < numOfRows;y++){
        for(int x = 0; x < numOfCols;x++){
            if(x < rightLim){
                highest = compare(highest,m[x][y]*m[x + 1][y]*m[x + 2][y]*m[x + 3][y]);
                if(y < downLim){
                    highest = compare(highest, m[x][y]*m[x +1][y+1]*m[x+2][y+2]*m[x+3][y+3]);
                }
            }
            if(y < downLim){
                highest =compare(highest, m[x][y]*m[x][y+1]*m[x][y+2]*m[x][y+3]);
                if(x > leftLim){
                    highest = compare(highest, m[x][y]*m[x - 1][y+1]*m[x-2][y+2]*m[x-3][y+3]);
                }
            }
        }
    }
    return highest;
}