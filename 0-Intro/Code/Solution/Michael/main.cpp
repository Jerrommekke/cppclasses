#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "traverse.h"

using Row    = std::vector<int>;
using Matrix = std::vector< Row >;

inline Matrix getMatrix(const std::string& filename)
{
    Matrix matrix;

    std::ifstream fileSream(filename);
    for (std::string line; getline(fileSream, line); )
    {
        matrix.push_back( Row() );
        auto currentRow = matrix.end();
        currentRow--;

        std::istringstream inputStream(line);
        int value;
        while (inputStream >> value)
            currentRow->push_back(value);

    }
    return matrix;
}

int main()
{
    Matrix matrix = getMatrix("grid.txt");
    int highest = search(matrix);
    std::cout << highest << std::endl;
    return 0;
}