#include <iostream>
#include <string>

#include "md5.h"


static std::vector<unsigned char> calculateMD5(const std::vector<unsigned char>& vec)
{
    MD5 md5;
    md5.update(vec.data(),  (MD5::size_type)vec.size());
    md5.finalize();
    return md5.hexbytes();

}


static std::string bytesToHex(std::vector<unsigned char> bytes)
{
    char buf[33];
    for (int i=0; i<16; i++)
        sprintf(buf+i*2, "%02x", bytes[i]);
    buf[32]=0;

    return std::string(buf);
}

static std::vector<unsigned char> combinedMD5(const std::vector<unsigned char>& one, const std::vector<unsigned char>& second)
{
    static std::vector<unsigned char> scratch(one.size());
    for (auto i = 0u; i < scratch.size(); ++i)
    {
        scratch[i] = one[i] | second[i];
    }

    return calculateMD5(scratch);
}

static std::vector<unsigned char> findConflict(const std::vector<unsigned char>& withBytes)
{
    return {{0, 2, 8, 135}};
}



int main(int argc, char* argv[]) {


    int numberOfZeroBits = 8;

    for (int i = 1; i < argc; ++i)
    {
        std::string s(argv[i]);

        // Convert to bytes
        std::vector<unsigned char> bytes( s.begin(), s.end());


        auto conflict = findConflict(bytes);


        std::cout << "MD5 of " << s << " is " << bytesToHex( calculateMD5(bytes) ) << "\n";
        std::cout << "The combined md5 is " << bytesToHex( combinedMD5(bytes, conflict) ) << "\n";

        auto md5bytes = combinedMD5(bytes,conflict);
        std::cout << "The combined md5 in bit representation is: ";
        for (auto& elem : md5bytes)
        {
            for (auto bit = 1u; bit <= 256; bit <<= 1)
            {
                std::cout << ((elem & bit) == bit);
            }
        }
        std::cout << std::endl;


        std::cout << "Hash conflict was found with: ";
        for (auto& elem : conflict)
        {
            std::cout << (int)elem << ", ";
        }
        std::cout << std::endl;

    }

    return 0;
}