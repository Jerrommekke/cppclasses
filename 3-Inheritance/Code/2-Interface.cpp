
#include <iostream>
#include <vector>

// This is a GOOD pure interface
class IRecorder
{
public:
    virtual ~IRecorder() {}
    virtual std::vector<int> getAudio() const = 0;
};

class IFancySomething
{
public:
    virtual void doSomething() const = 0;
};

class MicrophoneRecorder : public IRecorder, public IFancySomething
{
public:
    std::vector<int> getAudio() const
    {
        // get some audio from the mic
        return {1, 2, 3, 4};
    }

    void doSomething() const
    {
        // 
    }

};

class FileRecorder : public IRecorder
{
public:
    std::vector<int> getAudio() const
    {
        // Get some audio from a file
        return {5, 6, 7, 8};
    }
};


int main(int argc, char* argv[])
{
    IRecorder* recorder = nullptr;

    if (argc > 1 && std::string(argv[1]) == std::string("file") )
    {
        recorder = FileRecorder();
        std::cout << "Using file recorder..\n";
    } else {
        recorder = MicrophoneRecorder();
        std::cout << "Using mic recorder\n";
    }

    std::vector<int> samples = recorder->getAudio();
    
    std::cout << "Got the following samples: \n";
    for (auto& sample : samples)
        std::cout << sample << ", ";

    std::cout << std::endl;

    delete recorder;

}