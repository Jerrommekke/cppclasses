
#include <iostream>
#include <vector>

// This is a BAD 'interface' with default implementations
class IRecorder
{
public:
    
    // What happens if we forget the virtual destructor?
    virtual ~IRecorder() 
    {
        std::cout << "Base class gets cleaned up\n";
    }
    
    virtual std::vector<int> getAudio() const {
        return { 0, 0, 0, 0 }; // Return a default value 
    }
};


class MicrophoneRecorder : public IRecorder
{
public:
    ~MicrophoneRecorder() { 
        std::cout << "Mic class gets cleaned up\n";
    }
    std::vector<int> getAudio() const override
    {
        // get some audio from the mic
        return {1, 2, 3, 4};
    }
private:
    std::vector<int> somedata;
};

class FileRecorder : public IRecorder
{
public:
    std::vector<int> getAudio() const override
    {
        // Get some audio from a file
        return {5, 6, 7, 8};
    }
};


static void printSamples(const IRecorder& recorder)
{
    std::cout << "Got the following samples: \n";
    for (auto& sample : recorder.getAudio())
        std::cout << sample << ", ";
    std::cout << std::endl;
}


int main(int argc, char* argv[])
{
    std::unique_ptr<IRecorder> recorder = std::make_unique<IRecorder>();

    if (argc > 1 && std::string(argv[1]) == std::string("file") )
    {
        recorder = std::make_unique<FileRecorder>();
        std::cout << "Using file recorder..\n";
    } else {
        recorder = std::make_unique<MicrophoneRecorder>();
        std::cout << "Using mic recorder\n";
    }
    
    printSamples(*recorder);

}