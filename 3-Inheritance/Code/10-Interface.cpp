
#include <iostream>
#include <vector>

// This is a BAD 'interface' with default implementations
class IRecorder
{
public:
    
    // What happens if we forget the virtual destructor?
    
    virtual std::vector<int> getAudio() const {
        return { 0, 0, 0, 0 }; // Return a default value 
    }
};


class MicrophoneRecorder : public IRecorder
{
public:
    std::vector<int> getAudio() const override
    {
        // get some audio from the mic
        return {1, 2, 3, 4};
    }
};

class FileRecorder : public IRecorder
{
public:
    std::vector<int> getAudio() const override
    {
        // Get some audio from a file
        return {5, 6, 7, 8};
    }
};


static void printSamples(const IRecorder& recorder)
{
    std::cout << "Got the following samples: \n";
    for (auto& sample : recorder.getAudio())
        std::cout << sample << ", ";
    std::cout << std::endl;
}


static std::unique_ptr<IRecorder> createRecorder(bool file)
{
    if (file)
    {
        std::cout << "Using file recorder..\n";
        return std::make_unique<FileRecorder>();
    } else {
        std::cout << "Using mic recorder\n";
        return std::make_unique<MicrophoneRecorder>();
    }
}


int main(int argc, char* argv[])
{
    std::unique_ptr<IRecorder> recorder = createRecorder(argc > 1 && std::string(argv[1]) == std::string("file") ); 
    printSamples(*recorder);
}