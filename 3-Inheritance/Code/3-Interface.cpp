
#include <iostream>
#include <vector>

// This is a GOOD pure interface
class IRecorder
{
public:
    virtual ~IRecorder() {}
    virtual std::vector<int> getAudio() const = 0;
};


class MicrophoneRecorder : public IRecorder
{
public:
    std::vector<int> getAudio() const
    {
        // get some audio from the mic
        return {1, 2, 3, 4};
    }
};

class FileRecorder : public IRecorder
{
public:
    FileRecorder(const std::string& filename) {
        // open file here
    }

    std::vector<int> getAudio() const
    {
        // Get some audio from a file
        return {5, 6, 7, 8};
    }
};

void useRecorder(IRecorder* recorder)
{
    if (recorder == nullptr) {
        // Do some special thing here
    }

    // do some other stuff


}


int main(int argc, char* argv[])
{
    std::unique_ptr<IRecorder> recorder = nullptr;

    if (argc > 1 && std::string(argv[1]) == std::string("file") )
    {
        recorder = std::make_unique<FileRecorder>(filename);
        recorder = /* make some managed new */ new FileRecorder();
        std::cout << "Using file recorder..\n";
    } else {
        recorder = std::make_unique<MicrophoneRecorder>();
        std::cout << "Using mic recorder\n";
    }

    useRecorder(recorder.get());

    std::vector<int> samples = recorder->getAudio();
    
    std::cout << "Got the following samples: \n";
    for (auto& sample : samples)
        std::cout << sample << ", ";

    std::cout << std::endl;

}