
#include <iostream>
#include <vector>


class MicrophoneRecorder
{
public:
    std::vector<int> getAudio() const
    {
        // get some audio from the mic
        return {1, 2, 3, 4};
    }
};

class FileRecorder
{
public:
    std::vector<int> getAudio() const
    {
        // Get some audio from a file
        return {5, 6, 7, 8};
    }
};



int main(int argc, char* argv[])
{
    enum RecorderType {
        microphone,
        file
    };

    RecorderType rtype = microphone; // default

    if (argc > 1 && std::string(argv[1]) == std::string("file") )
    {
        rtype = file;
        std::cout << "Using file recorder..\n";
    } else {
        std::cout << "Using mic recorder\n";
    }

    std::vector<int> samples;
    switch (rtype)
    {
        case microphone:
        {
            MicrophoneRecorder recorder;
            samples = recorder.getAudio();
        }
            break;
        case file:
        {
            FileRecorder recorder;
            samples = recorder.getAudio();
        }
            break;
    }

    std::cout << "Got the following samples: \n";
    for (auto& sample : samples)
        std::cout << sample << ", ";

    std::cout << std::endl;

}