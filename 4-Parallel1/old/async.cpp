#include <thread>
#include <iostream>
#include <chrono>
#include <future>

using namespace std;


int veryExpensiveCalculation(int input)
{
    this_thread::sleep_for(chrono::seconds(3));
    return input*2;
}

int main()
{

    future<int> output1 = async( &veryExpensiveCalculation, 1);
    future<int> output2 = async( &veryExpensiveCalculation, 2);

    cout << "Expensive calculation 1 results in: " << output1.get() << std::endl;
    cout << "Expensive calculation 2 results in: " << output2.get() << std::endl;

}
