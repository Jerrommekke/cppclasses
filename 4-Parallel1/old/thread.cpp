#include <thread>
#include <iostream>
#include <chrono>

using namespace std;


void countDown(const std::string& name)
{
  for (int i = 3; i >= 0; --i) { 
    cout << name << ": " << i << "\n";
    this_thread::sleep_for(chrono::seconds(1));
  }

}


int main()
{

  thread thread1( bind( &countDown, "Thread 1") );
  thread thread2( bind( &countDown, "Thread 2") );
  

  thread1.join();
  thread2.join();

}
