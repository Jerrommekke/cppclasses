
#include <iostream>
#include <thread>


long long consecutiveSum(long long number)
{
    auto sum = 0l;
    for (auto i = 1l; i <= number; ++i)
        sum += i;
    return sum;
}


int main()
{
    long long num1 = 300000000l;
    long long num2 = 500000000l;

    long long sum1 = 0l;
    long long sum2 = 0l;

    std::thread work( [&sum1, &sum2, num1, num2]() { 
            sum1 = consecutiveSum(num1); 
            sum2 = consecutiveSum(num2); 
        } );

    work.join();
    
    std::cout << "Consecutive sum to " << num1 << " is: " << sum1 <<"\n";
    std::cout << "Consecutive sum to " << num2 << " is: " << sum2 <<"\n";
}