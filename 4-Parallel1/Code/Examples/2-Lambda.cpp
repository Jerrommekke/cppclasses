
#include <iostream>


void sayHi(const std::string& person)
{
    std::cout << "Hi, " << person << "\n";
}


int main()
{
    // when does this memory get deleted?
    std::string ppl = "guys!"; 

    // or better C++11: 
    auto func = [=]() { sayHi(ppl); };
    func();

    // What just happened!?

    // And what happens now?
    ppl = "girls!";
    func();
}