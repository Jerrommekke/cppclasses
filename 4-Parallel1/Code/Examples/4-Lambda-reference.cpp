
#include <iostream>


void sayHi(const std::string& person)
{
    std::cout << "Hi, " << person << "\n";
}

// Imagine this is a lambda!
class sayHiRunnable // (because we all love Java!)
{
public:
    sayHiRunnable(std::string& person) : person_(person) {}

    void operator()() { 
        sayHi(person_);
    }
private:
    std::string& person_;
};



int main()
{
        
        // when does this memory get deleted?
        std::string ppl = "guys!"; 

        // or better C++11:
        auto func         = [&]() { sayHi(ppl); };
        auto oldSkoolFunc = sayHiRunnable(ppl);

        // Awesome!
        ppl = "girls!";
        func();
        oldSkoolFunc();


        // So really, this means:
        auto lambda =  // Create a functor:
                    [&] // pass the *context* (i.e. everything needed) by reference
                    () // take no arguments
                    { sayHi(ppl); } // this is the function body
                    ; // this closes the assignment to "lambda"

    

    // Note: no context needed - pure function
    auto lambda_with_args = [](const std::string& person) { sayHi(person); };

    ppl = "lambdas!";
    lambda();
    lambda_with_args("everyone!");




    return 0;
}