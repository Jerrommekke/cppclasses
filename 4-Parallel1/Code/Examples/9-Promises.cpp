
#include <iostream>
#include <thread>
#include <future>


long long consecutiveSum(long long number)
{
    auto sum = 0l;
    for (auto i = 1l; i <= number; ++i)
        sum += i;
    return sum;
}

int main()
{
    long long num1 = 300000000l;
    long long num2 = 500000000l;

    std::promise<long long> p1, p2;
    std::future<long long> f1 = p1.get_future();
    std::future<long long> f2 = p2.get_future();

    std::thread work( [num1, num2, prom1 = std::move(p1), prom2 = std::move(p2)]() mutable  { 
        prom1.set_value( consecutiveSum(num1) ); 
        prom2.set_value( consecutiveSum(num2) ); 
        } );
    
    std::cout << "Consecutive sum to " << num1 << " is: " << f1.get() <<"\n";
    std::cout << "Consecutive sum to " << num2 << " is: " << f2.get() <<"\n";
    
    work.join();
}