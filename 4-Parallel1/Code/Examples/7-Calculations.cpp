
#include <iostream>
#include <thread>


long long consecutiveSum(long long number)
{
    auto sum = 0l;
    for (auto i = 1l; i <= number; ++i)
        sum += i;
    return sum;
}


int main()
{
    long long num = 1000000l;
    long long sum = 0l;

    std::thread work( [&]() { sum = consecutiveSum(num); } );
    // Some work here

    // now let's access sum?
    sum = sum + 1;
    std::cout << "Work is done, resulted in: " << sum << "\n";

    work.join();
    
    
}