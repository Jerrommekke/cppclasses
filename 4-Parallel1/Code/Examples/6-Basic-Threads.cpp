
#include <iostream>
#include <thread>

void sayHi(const std::string& person)
{
    // IMPORTANT: the function does NOT define
    // in which thread this code gets executed
    // (nor does a class)
    std::cout << "Hi, " << person << "\n";
}

int main()
{
    std::string ppl1 = "guys!"; 
    std::string ppl2 = "girls!";

    std::thread thread1( [=]() {  sayHi(ppl1); } );
    std::thread thread2( [=]() {  sayHi(ppl2); } );
    // Only the code within the context (scope) of the lambdas executes in a different thread

    thread1.join();
    thread2.join();
}