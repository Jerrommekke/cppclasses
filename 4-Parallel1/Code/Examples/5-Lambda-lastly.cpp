
#include <iostream>


void sayHi(const std::string& person)
{
    std::cout << "Hi, " << person << "\n";
}

int main()
{
    // when does this memory get deleted?
    std::string ppl1 = "guys!"; 
    std::string ppl2 = "girls!";

    // or better C++11:
    //auto func = &sayHi;
    auto func   = [&ppl1, ppl2] // pass ppl1 by reference, and ppl2 by value
                    () { 
                        sayHi(ppl1);
                        sayHi(ppl2);
                         };
    
    ppl1 = "lambdas!";
    ppl2 = "everyone!";

    func();

    // Also, think about processor steps. Which line gets executed when?
}