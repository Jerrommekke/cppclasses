
#include <iostream>


void sayHi(const std::string& person)
{
    std::cout << "Hi, " << person << "\n";
}

// Imagine this is a lambda!
class sayHiRunnable // (because we all love Java!)
{
public:
    sayHiRunnable(const std::string& person) : person_(person) {}

    void operator()() { 
        sayHi(person_);
    }
private:
    std::string person_;
};


int main()
{
    // when does this memory get deleted?
    std::string ppl = "guys!"; 

    // or better C++11:
    //auto func = &sayHi;
    auto func         = [=]() { sayHi(ppl); };
    auto oldSkoolFunc = sayHiRunnable(ppl);

    // Awesome!
    func();
    oldSkoolFunc();
}