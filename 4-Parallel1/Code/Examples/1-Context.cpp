
#include <iostream>

void sayHi(const std::string& person)
{
    std::cout << "Hi, " << person << "\n";
}

int main()
{
    //{        
        // when does this memory get deleted?
        std::string ppl = "guys!"; 
        sayHi(ppl);
    //}
    

    // Function pointers
    auto func = &sayHi;
    func(ppl);
}