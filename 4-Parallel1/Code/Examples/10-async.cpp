
#include <iostream>
#include <thread>
#include <future>


long long consecutiveSum(long long number)
{
    auto sum = 0l;
    for (auto i = 1l; i <= number; ++i)
        sum += i;
    return sum;
}

int main()
{
    long long num1 = 300000000l;
    long long num2 = 500000000l;

    auto f1 = std::async( [num1]() { return consecutiveSum(num1); });
    auto f2 = std::async( [num2]() { return consecutiveSum(num2); });

    std::cout << "Consecutive sum to " << num2 << " is: " << f2.get() <<"\n";
    std::cout << "Consecutive sum to " << num1 << " is: " << f1.get() <<"\n";
    
    
}