
class Listener;


class Fingerprinter : public Notifier
{
public:

    Fingerprinter(Listener *listener) : listener_(listener) {}
    
    void doSomething()
    {
        Fingerprint fp = ... 
        listener_->update();

        // can I now clear up fp?
    }

    Fingerprint *getCurrentFingerprint();

    Configuration getCurrentConfiguration();

private:
    Listener* listener_;
    
};


class Uploader : public Listener
{
public:
    virtual void update()
    {
        Fingerprint* created = fingerprinter_->getCurrentFingerprint();
        fingerprinter_->statusChanged();
        .. do something
    }
}